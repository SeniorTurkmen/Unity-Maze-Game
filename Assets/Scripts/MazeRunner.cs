using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeRunner : MonoBehaviour
{

    [SerializeField]
    private int _velocity = 20;
    private Vector3 move;

    private Rigidbody rb;


    // İlk başlatma sırasında çalışır
    void Start()
    {
        rb = GetComponent<Rigidbody>(); //Fiziksel Özellikleri kullanabilmek için
                                        //oyun başlangıcında Rigidbody özelliğini çalıştırıyoruz.
    }

    // Her kare başladığında çalışır
    void Update()
    {
        Controller();

    }
    void Controller()
    {
        float Vertical = Input.GetAxis("Vertical"); //Kullanıcıdan dikey yönde veri alır. ( "up||down" keys || "w || s" keys)
        // float Horizontal = Input.GetAxis("Horizontal"); //Kullanıcıdan yatay yönde veri alır. ( "right||left" keys || "a || d" keys)
        transform.Rotate(new Vector3(0f,Input.GetAxis("Mouse X"),0f) *Time.deltaTime * 40);
        //alınan verileri 3 Boyutlu vector olarak saklıyoruz.
        if(Input.GetKey(KeyCode.UpArrow)||Input.GetKey("w")){
            Vector3 move = transform.rotation * Vector3.forward * _velocity;
            rb.AddForce(move);//Tutulan 3 Boyutlu Vektörü Unity'nin fizik motoruna, uygulaması gerken kuvvet cinsinden gönderiyoruz.
        }
        if(Input.GetKey(KeyCode.DownArrow)||Input.GetKey("s")){
            Vector3 move = (transform.rotation * Vector3.back * _velocity);
            rb.AddForce(move);//Tutulan 3 Boyutlu Vektörü Unity'nin fizik motoruna, uygulaması gerken kuvvet cinsinden gönderiyoruz.
        }
        if(Input.GetKey(KeyCode.LeftArrow)||Input.GetKey("a")){
            Vector3 move = (transform.rotation * Vector3.left * _velocity);
            rb.AddForce(move);//Tutulan 3 Boyutlu Vektörü Unity'nin fizik motoruna, uygulaması gerken kuvvet cinsinden gönderiyoruz.
        }
        if(Input.GetKey(KeyCode.RightArrow)||Input.GetKey("d")){
            Vector3 move = (transform.rotation * Vector3.right * _velocity);
            rb.AddForce(move);//Tutulan 3 Boyutlu Vektörü Unity'nin fizik motoruna, uygulaması gerken kuvvet cinsinden gönderiyoruz.
        }

        if(transform.position.y < 0.3f)
        {
            if(Input.GetKey(KeyCode.Space))
            {
                Vector3 move = new Vector3(0, 200, 0);
                rb.AddForce(move);//Tutulan 3 Boyutlu Vektörü Unity'nin fizik motoruna, uygulaması gerken kuvvet cinsinden gönderiyoruz.
            }
        }
    }
}
